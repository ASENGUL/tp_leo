Commande pour des toutes les infos sur le réseau sous windows : ipconfig /all

Informations demandés :

Int wifi {

    ip : 10.33.18.119

    Physical Address : 3C-9C-0F-C7-ES-20

    Default Gateway 10.33.19.254
}

Int Ethernet {

    ip : 192.168.56.1

    Physical Address : 0A-00-27-00-00-10
}

A QUOI SERT LA GATEWAY D'YNOV ?

La Gateway permet la communication de toutes les machines chez Ynov.


------------------------------------------------------------------------------
------------------------------------------------------------------------------


II. Modifications des informations

1. Settings > Network & internet.

2. select Wi-Fi > Manage known networks. Choose the network for which you want to change the settings.

Ip modificated {

    IP : 10.33.18.122

    SUBNET PREFIX LENGTH : 24

    DEFAULT GATEWAY : 10.33.19.254

    PREFERRED DNS : 10.33.18.122

    ALTERNATE DNS : 10.33.18.21
}

QUESTION. 
Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou
non, expliquez pourquoi c'est possible de perdre son accès internet en faisant
cette opération.

Réponse.
Il est possible qu'on perde la connexion car l'adresse ip choisis peut être déjà
utilisée. En informatique si une ip est déjà utilisée et qu'un nouveaux client
avec la même ip se connecte, il pourra envoyer des requêtes mais toutes les réponses
seront envoyé à la première machine ayant utilisée l'ip.

------------------------------------------------------------------------------
------------------------------------------------------------------------------

III. Modification d'adresse IP

Mon ip réseau Ethernet {

    IP : 192.168.0.1

    SUBNET PREFIX LENGTH : 32

    DEFAULT GATEWAY : 192.168.0.3

    PREFERRED DNS : 192.168.0.1

    ALTERNATE DNS : 192.168.0.2
}

Ip réseau Ethernet Lucas {
        
    IP : 192.168.0.2

    SUBNET PREFIX LENGTH : 32

    DEFAULT GATEWAY : 192.168.0.3

    PREFERRED DNS : 192.168.0.2

    ALTERNATE DNS : 192.168.0.1
}

Nos machines sont maintenant connectés au même réseau. Nous pouvons nous ping entre nous
comme nous pouvons le voir ci dessous :

Ping statistics for 192.168.0.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 1ms, Maximum = 1ms, Average = 1ms

Table ARP :

Interface: 192.168.0.1 --- 0x11
  Internet Address      Physical Address      Type
  192.168.0.2           38-f3-ab-90-59-41     dynamic
  192.168.0.3           ff-ff-ff-ff-ff-ff     static

Ici je peux voir l'ip de mon camarade et son adresse mac ainsi que des informations sur
la Gateway

------------------------------------------------------------------------------
------------------------------------------------------------------------------

IV. Utilisation d'un des deux comme gateway

- On 
ping
 le pc connecté au notre 
  - sur le PC qui n'a plus internet
    - On met la passerelle du pc etant connecté "192.168.137.1".
  - sur le PC qui a toujours internet
    - On partage la connexion via le centre de réseau et partage et on active le partage Ethernet dans les propriété du Wifi.

---

- Pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu
On 
ping
 le serveur 8.8.8.8 et "google.com"
- Utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)
On utilise 
tracert 8.8.8.8
 pour pouvoir prouver que la demande passe par l'IP du pc fournissant la connexion
